<?php

require '../vendor/autoload.php';

function test(int $x, int $y, int ...$rest): array
{
    return $rest;
}

$a = [1, 2, 3, 4, 5];
$b = [6, 7, 8, 9, 10, 11];
$c = [100, 1002, ...$a, ...$b];

$sum = array_reduce($c, fn ($carry, $item) => $carry += $item);

dd($sum);
