<?php

namespace App;

class Email
{
    protected $email;

    public function __construct($email)
    {
        $this->email = $email;
    }

    public function send()
    {
        return $this->email;
    }
}
