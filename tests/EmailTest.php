<?php

use App\Email;
use PHPUnit\Framework\TestCase;

require 'vendor/autoload.php';

class EmailTest extends TestCase
{
    public function testCanSendEmail(): void
    {
        $email = new Email('christian@egol.de');
        $response = $email->send();
        $this->assertEquals('christian@egol.de', $response);
    }
}
